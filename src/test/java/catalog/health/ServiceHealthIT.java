package catalog.health;

import catalog.item.Item;
import catalog.item.TestConfiguration;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.client.RestClientException;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(TestConfiguration.class)
public class ServiceHealthIT {

    @LocalServerPort
    private int port;

    @Autowired
    private MockRestServiceServer server;

    @Value("classpath:/itunes_searchSinatra.json")
    Resource resourceItunes_searchSinatra;

    @Value("classpath:/googlebooks_searchSinatra.json")
    Resource resourceGooglebooks_searchSinatra;

    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    public void testServiceStatusUp() {
        ResponseEntity<HealthResponse> response = testRestTemplate.getForEntity("http://localhost:" + port + "/actuator/health", HealthResponse.class);

        HealthResponse healthResponse = response.getBody();
        assertEquals(Status.UP, healthResponse.getStatus());

        Health health = healthResponse.getDetails().get("itunes");
        assertEquals(Status.UP, health.getStatus());

        health = healthResponse.getDetails().get("googleBooks");
        assertEquals(Status.UP, health.getStatus());
    }

    @Test
    public void testUpstreamServicesStatusDownAfterError() {
        upstreamServicesReturnError();

        ResponseEntity<HealthResponse> response = testRestTemplate.getForEntity("http://localhost:" + port + "/actuator/health", HealthResponse.class);

        HealthResponse healthResponse = response.getBody();
        assertEquals(Status.UP, healthResponse.getStatus());

        Health health = healthResponse.getDetails().get("itunes");
        assertEquals(Status.DOWN, health.getStatus());

        health = healthResponse.getDetails().get("googleBooks");
        assertEquals(Status.DOWN, health.getStatus());
    }

    @Test
    public void testUpstreamServicesStatusUpAfterSuccess() {
        upstreamServicesReturnErrorThenSuccess();

        ResponseEntity<HealthResponse> response = testRestTemplate.getForEntity("http://localhost:" + port + "/actuator/health", HealthResponse.class);

        HealthResponse healthResponse = response.getBody();
        assertEquals(Status.UP, healthResponse.getStatus());

        Health health = healthResponse.getDetails().get("itunes");
        assertEquals(Status.UP, health.getStatus());

        health = healthResponse.getDetails().get("googleBooks");
        assertEquals(Status.UP, health.getStatus());
    }

    private void upstreamServicesReturnError() throws RestClientException {
        server.expect(requestTo("https://itunes.apple.com/search?term=zaqxsw&media=music&entity=album&limit=5"))
                .andRespond(withBadRequest());
        server.expect(requestTo("https://www.googleapis.com/books/v1/volumes?q=zaqxsw&maxResults=5"))
                .andRespond(withBadRequest());

        testRestTemplate
                .exchange("http://localhost:" + port + "/item/search?phrase=zaqxsw", HttpMethod.GET, null, new ParameterizedTypeReference<List<Item>>() {
                });

        server.verify();
    }

    private void upstreamServicesReturnErrorThenSuccess() throws RestClientException {
        server.expect(requestTo("https://itunes.apple.com/search?term=zaqxsw&media=music&entity=album&limit=5"))
                .andRespond(withBadRequest());
        server.expect(requestTo("https://www.googleapis.com/books/v1/volumes?q=zaqxsw&maxResults=5"))
                .andRespond(withBadRequest());
        server.expect(requestTo("https://itunes.apple.com/search?term=Sinatra&media=music&entity=album&limit=5"))
                .andRespond(withSuccess(resourceItunes_searchSinatra, new MediaType("text", "javascript")));
        server.expect(requestTo("https://www.googleapis.com/books/v1/volumes?q=Sinatra&maxResults=5"))
                .andRespond(withSuccess(resourceGooglebooks_searchSinatra, new MediaType("application", "json")));

        testRestTemplate
                .exchange("http://localhost:" + port + "/item/search?phrase=zaqxsw", HttpMethod.GET, null, new ParameterizedTypeReference<List<Item>>() {
                });
        testRestTemplate
                .exchange("http://localhost:" + port + "/item/search?phrase=Sinatra", HttpMethod.GET, null, new ParameterizedTypeReference<List<Item>>() {
                });

        server.verify();
    }

}
