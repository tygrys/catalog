package catalog.health;

import java.util.Map;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.actuate.health.Status;

@Data
@NoArgsConstructor
public class HealthResponse {

    private Status status;
    private Map<String, Health> details;

}
