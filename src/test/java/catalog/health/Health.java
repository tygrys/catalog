package catalog.health;

import java.util.Map;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.actuate.health.Status;

@Data
@NoArgsConstructor
public class Health {

    private Status status;
    private Map<String, Object> details;
    
}
