package catalog.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {
    
    @Autowired
    RestTemplate restTemplate;
    
    @Bean
    public MockRestServiceServer mockRestServiceServer() {
        return MockRestServiceServer.createServer(restTemplate);
    }

}
