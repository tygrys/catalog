package catalog.item;

import static catalog.item.TestData.GOOGLEBOOKS_ITEMS;
import static catalog.item.TestData.ITUNES_ITEMS;
import static catalog.item.TestData.SORTED_GOOGLEBOOKS_ITEMS;
import static catalog.item.TestData.SORTED_ITEMS;
import static catalog.item.TestData.SORTED_ITUNES_ITEMS;
import java.util.List;
import java.util.function.Consumer;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ItemAggregatorTest {

    Consumer<List<Item>> consumerMock = mock(Consumer.class);
    ItemAggregator instance = new ItemAggregator(consumerMock);

    @Test
    public void testAggregateItunesThenGoogleBooksItems() {
        instance.aggregateItunesItems(ITUNES_ITEMS);

        assertFalse(instance.areConsumed());
        verifyNoMoreInteractions(consumerMock);

        instance.aggregateGoogleBooksItems(GOOGLEBOOKS_ITEMS);

        ArgumentCaptor<List<Item>> argumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(consumerMock).accept(argumentCaptor.capture());
        assertEquals(SORTED_ITEMS, argumentCaptor.getValue());
        assertTrue(instance.areConsumed());
    }

    @Test
    public void testAggregateGoogleBooksThenItunesItems() {
        instance.aggregateGoogleBooksItems(GOOGLEBOOKS_ITEMS);

        assertFalse(instance.areConsumed());
        verifyNoMoreInteractions(consumerMock);

        instance.aggregateItunesItems(ITUNES_ITEMS);

        ArgumentCaptor<List<Item>> argumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(consumerMock).accept(argumentCaptor.capture());
        assertEquals(SORTED_ITEMS, argumentCaptor.getValue());
        assertTrue(instance.areConsumed());
    }

    @Test
    public void testAggregateItunesItemsThenForceConsume() {
        instance.aggregateItunesItems(ITUNES_ITEMS);

        verifyNoMoreInteractions(consumerMock);

        instance.forceConsume();

        ArgumentCaptor<List<Item>> argumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(consumerMock).accept(argumentCaptor.capture());
        assertEquals(SORTED_ITUNES_ITEMS, argumentCaptor.getValue());
        assertTrue(instance.areConsumed());
    }

    @Test
    public void testAggregateGoogleBooksItemsThenForceCnsume() {
        instance.aggregateGoogleBooksItems(GOOGLEBOOKS_ITEMS);

        verifyNoMoreInteractions(consumerMock);

        instance.forceConsume();

        ArgumentCaptor<List<Item>> argumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(consumerMock).accept(argumentCaptor.capture());
        assertEquals(SORTED_GOOGLEBOOKS_ITEMS, argumentCaptor.getValue());
        assertTrue(instance.areConsumed());
    }
}
