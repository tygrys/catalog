package catalog.item;

import java.util.Collections;
import org.junit.jupiter.api.Test;
import static catalog.item.TestData.ITEMS;
import static catalog.item.TestData.SORTED_ITEMS;
import java.util.LinkedList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemTest {

    @Test
    public void testCompareTo() {
        LinkedList items = new LinkedList(ITEMS);
        Collections.sort(items);
        assertEquals(SORTED_ITEMS, items);
    }

}
