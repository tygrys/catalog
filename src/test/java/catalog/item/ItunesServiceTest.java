package catalog.item;

import static catalog.item.TestData.ITUNES_ITEMS;
import static catalog.item.TestData.ITUNES_RESULTS;
import static catalog.item.TestData.SEARCH_PHRASE;
import java.net.URI;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ItunesServiceTest {

    public final RestTemplate restTemplateMock = mock(RestTemplate.class);
    public final ItunesService instance = new ItunesService();

    public ItunesServiceTest() {
        instance.restTemplate = restTemplateMock;
    }
    
    @Test
    public void testSearchReturnsNoItems() {
        when(restTemplateMock.getForEntity(any(URI.class), eq(ItunesResponse.class)))
                .thenReturn(new ResponseEntity<>(ItunesResponse.builder()
                        .resultCount(0)
                        .results(List.of()).build(), HttpStatus.OK));

        List<Item> items = instance.search("zaqxsw");

        assertTrue(items.isEmpty());
    }

    @Test
    public void testSearchReturnsItems() {
        when(restTemplateMock.getForEntity(any(URI.class), eq(ItunesResponse.class)))
                .thenReturn(new ResponseEntity<>(
                        ItunesResponse.builder()
                                .resultCount(ITUNES_RESULTS.size())
                                .results(ITUNES_RESULTS).build(), HttpStatus.OK));

        List<Item> items = instance.search(SEARCH_PHRASE);

        List<Item> expectedItems = ITUNES_ITEMS;
        assertEquals(expectedItems, items);
    }

}
