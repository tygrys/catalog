package catalog.item;

import static catalog.item.TestData.SORTED_GOOGLEBOOKS_ITEMS;
import static catalog.item.TestData.SORTED_ITEMS;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Import(TestConfiguration.class)
public class ItemSearchIT {

    @LocalServerPort
    private int port;

    @Autowired
    private MockRestServiceServer server;

    @Value("classpath:/itunes_searchZaqxsw.json")
    Resource resourceItunes_searchZaqxsw;
    
    @Value("classpath:/itunes_searchSinatra.json")
    Resource resourceItunes_searchSinatra;

    @Value("classpath:/googlebooks_searchzaqxsw.json")
    Resource resourceGooglebooks_searchzaqxsw;
    
    @Value("classpath:/googlebooks_searchSinatra.json")
    Resource resourceGooglebooks_searchSinatra;

    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    public void testNoPhraseReturnsError() throws Exception {
        ResponseEntity<Object> response = testRestTemplate.getForEntity("http://localhost:" + port + "/item/search", Object.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void testPhraseReturnsNoItems() throws Exception {
        server.expect(requestTo("https://itunes.apple.com/search?term=zaqxsw&media=music&entity=album&limit=5"))
                .andRespond(withSuccess(resourceItunes_searchZaqxsw, new MediaType("text", "javascript")));
        server.expect(requestTo("https://www.googleapis.com/books/v1/volumes?q=zaqxsw&maxResults=5"))
                .andRespond(withSuccess(resourceGooglebooks_searchzaqxsw, new MediaType("application", "json")));

        ResponseEntity<List<Item>> response = testRestTemplate.exchange("http://localhost:" + port + "/item/search?phrase=zaqxsw", HttpMethod.GET, null, new ParameterizedTypeReference<List<Item>>() {
        });
        
        server.verify();

        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertTrue(response.getBody().isEmpty());
    }

    @Test
    public void testPhraseResultsFromItunesAndGoogleBooksAreReturned() throws Exception {
        server.expect(requestTo("https://itunes.apple.com/search?term=Sinatra&media=music&entity=album&limit=5"))
                .andRespond(withSuccess(resourceItunes_searchSinatra, new MediaType("text", "javascript")));
        server.expect(requestTo("https://www.googleapis.com/books/v1/volumes?q=Sinatra&maxResults=5"))
                .andRespond(withSuccess(resourceGooglebooks_searchSinatra, new MediaType("application", "json")));

        ResponseEntity<List<Item>> response = testRestTemplate
                .exchange("http://localhost:" + port + "/item/search?phrase=Sinatra", HttpMethod.GET, null, new ParameterizedTypeReference<List<Item>>() {});
        
        server.verify();
        
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        List<Item> items = response.getBody();
        assertNotNull(items);
        assertEquals(SORTED_ITEMS, items);
    }

    @Test
    public void testPhraseResultsFromGoogleBooksAreReturnedWhenItunesReturnsError() throws Exception {
        server.expect(requestTo("https://itunes.apple.com/search?term=Sinatra&media=music&entity=album&limit=5"))
                .andRespond(withBadRequest());
        server.expect(requestTo("https://www.googleapis.com/books/v1/volumes?q=Sinatra&maxResults=5"))
                .andRespond(withSuccess(resourceGooglebooks_searchSinatra, new MediaType("application", "json")));

        ResponseEntity<List<Item>> response = testRestTemplate
                .exchange("http://localhost:" + port + "/item/search?phrase=Sinatra", HttpMethod.GET, null, new ParameterizedTypeReference<List<Item>>() {});
        
        server.verify();
        
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        List<Item> items = response.getBody();
        assertNotNull(items);
        assertEquals(SORTED_GOOGLEBOOKS_ITEMS, items);
    }

}
