package catalog.item;

import static catalog.item.TestData.*;
import java.util.List;
import java.util.function.Consumer;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.springframework.web.context.request.async.DeferredResult;

public class ItemControllerTest {
    
    public ItemControllerTest() {
    }

    @Test
    public void testSearchItems() {
        ItemController instance = new ItemController();
        
        ItemService itemServiceMock = mock(ItemService.class);
        instance.itemService = itemServiceMock;
                
        DeferredResult<List<Item>> result = instance.search(SEARCH_PHRASE);
        
        ArgumentCaptor<Consumer> consumerCaptor = ArgumentCaptor.forClass(Consumer.class);
        verify(itemServiceMock, times(1)).search(eq(SEARCH_PHRASE), consumerCaptor.capture());

        consumerCaptor.getValue().accept(ITEMS);
        assertEquals(ITEMS, result.getResult());
    }
    
}
