package catalog.item;

import java.util.List;

public class TestData {

    static final String ITUNES_TITLE_5 = "Sinatra/Jobim: The Complete Reprise Recordings";
    static final String ITUNES_TITLE_4 = "Sinatra At the Sands";
    static final String ITUNES_TITLE_3 = "Live and Swingin\': The Ultimate Rat Pack Collection";
    static final String ITUNES_TITLE_2 = "Ultimate Sinatra";
    static final String ITUNES_TITLE_1 = "Nothing But the Best (Remastered)";
    static final String ITUNES_AUTHOR_5 = "Frank Sinatra & Antônio Carlos Jobim";
    static final String ITUNES_AUTHOR_4 = "Frank Sinatra";
    static final String ITUNES_AUTHOR_3 = "Frank Sinatra, Dean Martin & Sammy Davis, Jr.";
    static final String ITUNES_AUTHOR_2 = "Frank Sinatra";
    static final String ITUNES_AUTHOR_1 = "Frank Sinatra";
    static final List<ItunesResult> ITUNES_RESULTS = List.of(
            ItunesResult.builder().artistName(ITUNES_AUTHOR_1).collectionName(ITUNES_TITLE_1).build(),
            ItunesResult.builder().artistName(ITUNES_AUTHOR_2).collectionName(ITUNES_TITLE_2).build(),
            ItunesResult.builder().artistName(ITUNES_AUTHOR_3).collectionName(ITUNES_TITLE_3).build(),
            ItunesResult.builder().artistName(ITUNES_AUTHOR_4).collectionName(ITUNES_TITLE_4).build(),
            ItunesResult.builder().artistName(ITUNES_AUTHOR_5).collectionName(ITUNES_TITLE_5).build());

    static final String GOOGLEBOOKS_TITLE_5 = "Frank Sinatra";
    static final String GOOGLEBOOKS_TITLE_4 = "Frank Sinatra";
    static final String GOOGLEBOOKS_TITLE_3 = "Sinatra";
    static final String GOOGLEBOOKS_TITLE_2 = "Frank Sinatra, My Father";
    static final String GOOGLEBOOKS_TITLE_1 = "Frank Sinatra";
    static final String GOOGLEBOOKS_AUTHOR_5_1 = "Jeanne Fuchs";
    static final String GOOGLEBOOKS_AUTHOR_5_2 = "Jeanne Thomas Fuchs";
    static final String GOOGLEBOOKS_AUTHOR_5_3 = "Ruth Prigozy";
    static final String GOOGLEBOOKS_AUTHOR_5 = GOOGLEBOOKS_AUTHOR_5_1 + ", " + GOOGLEBOOKS_AUTHOR_5_2 + ", " + GOOGLEBOOKS_AUTHOR_5_3;
    static final String GOOGLEBOOKS_AUTHOR_4 = "Chris Ingham";
    static final String GOOGLEBOOKS_AUTHOR_3 = "Tim Knight";
    static final String GOOGLEBOOKS_AUTHOR_2 = "Nancy Sinatra";
    static final String GOOGLEBOOKS_AUTHOR_1 = "John Frayn Turner";
    static final GoogleItem[] GOOGLEBOOKS_RESULTS = new GoogleItem[]{
        GoogleItem.builder().volumeInfo(GoogleVolumeInfo.builder().authors(new String[]{GOOGLEBOOKS_AUTHOR_1}).title(GOOGLEBOOKS_TITLE_1).build()).build(),
        GoogleItem.builder().volumeInfo(GoogleVolumeInfo.builder().authors(new String[]{GOOGLEBOOKS_AUTHOR_2}).title(GOOGLEBOOKS_TITLE_2).build()).build(),
        GoogleItem.builder().volumeInfo(GoogleVolumeInfo.builder().authors(new String[]{GOOGLEBOOKS_AUTHOR_3}).title(GOOGLEBOOKS_TITLE_3).build()).build(),
        GoogleItem.builder().volumeInfo(GoogleVolumeInfo.builder().authors(new String[]{GOOGLEBOOKS_AUTHOR_4}).title(GOOGLEBOOKS_TITLE_4).build()).build(),
        GoogleItem.builder().volumeInfo(GoogleVolumeInfo.builder().authors(new String[]{GOOGLEBOOKS_AUTHOR_5_1, GOOGLEBOOKS_AUTHOR_5_2, GOOGLEBOOKS_AUTHOR_5_3}).title(GOOGLEBOOKS_TITLE_5).build()).build()};

    static final List<Item> ITUNES_ITEMS = List.of(
            new Item(ITUNES_TITLE_1, ITUNES_AUTHOR_1, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_2, ITUNES_AUTHOR_2, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_3, ITUNES_AUTHOR_3, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_4, ITUNES_AUTHOR_4, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_5, ITUNES_AUTHOR_5, Item.Type.ALBUM));

    static final List<Item> SORTED_ITUNES_ITEMS = List.of(
            new Item(ITUNES_TITLE_3, ITUNES_AUTHOR_3, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_1, ITUNES_AUTHOR_1, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_4, ITUNES_AUTHOR_4, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_5, ITUNES_AUTHOR_5, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_2, ITUNES_AUTHOR_2, Item.Type.ALBUM));

    static final List<Item> GOOGLEBOOKS_ITEMS = List.of(
            new Item(GOOGLEBOOKS_TITLE_1, GOOGLEBOOKS_AUTHOR_1, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_2, GOOGLEBOOKS_AUTHOR_2, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_3, GOOGLEBOOKS_AUTHOR_3, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_4, GOOGLEBOOKS_AUTHOR_4, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_5, GOOGLEBOOKS_AUTHOR_5, Item.Type.BOOK));

    static final List<Item> SORTED_GOOGLEBOOKS_ITEMS = List.of(
            new Item(GOOGLEBOOKS_TITLE_4, GOOGLEBOOKS_AUTHOR_4, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_5, GOOGLEBOOKS_AUTHOR_5, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_1, GOOGLEBOOKS_AUTHOR_1, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_2, GOOGLEBOOKS_AUTHOR_2, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_3, GOOGLEBOOKS_AUTHOR_3, Item.Type.BOOK));

    static final List<Item> ITEMS = List.of(
            new Item(ITUNES_TITLE_1, ITUNES_AUTHOR_1, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_2, ITUNES_AUTHOR_2, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_3, ITUNES_AUTHOR_3, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_4, ITUNES_AUTHOR_4, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_5, ITUNES_AUTHOR_5, Item.Type.ALBUM),
            new Item(GOOGLEBOOKS_TITLE_1, GOOGLEBOOKS_AUTHOR_1, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_2, GOOGLEBOOKS_AUTHOR_2, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_3, GOOGLEBOOKS_AUTHOR_3, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_4, GOOGLEBOOKS_AUTHOR_4, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_5, GOOGLEBOOKS_AUTHOR_5, Item.Type.BOOK));

    static final List<Item> SORTED_ITEMS = List.of(
            new Item(GOOGLEBOOKS_TITLE_4, GOOGLEBOOKS_AUTHOR_4, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_5, GOOGLEBOOKS_AUTHOR_5, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_1, GOOGLEBOOKS_AUTHOR_1, Item.Type.BOOK),
            new Item(GOOGLEBOOKS_TITLE_2, GOOGLEBOOKS_AUTHOR_2, Item.Type.BOOK),
            new Item(ITUNES_TITLE_3, ITUNES_AUTHOR_3, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_1, ITUNES_AUTHOR_1, Item.Type.ALBUM),
            new Item(GOOGLEBOOKS_TITLE_3, GOOGLEBOOKS_AUTHOR_3, Item.Type.BOOK),
            new Item(ITUNES_TITLE_4, ITUNES_AUTHOR_4, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_5, ITUNES_AUTHOR_5, Item.Type.ALBUM),
            new Item(ITUNES_TITLE_2, ITUNES_AUTHOR_2, Item.Type.ALBUM));

    static final String SEARCH_PHRASE = "Sinatra";

}
