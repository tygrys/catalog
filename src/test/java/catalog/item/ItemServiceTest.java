package catalog.item;

import static catalog.item.TestData.*;
import java.util.List;
import java.util.function.Consumer;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.core.task.TaskExecutor;

public class ItemServiceTest {

    private final ItunesService itunesService = mock(ItunesService.class);
    private final GoogleBooksService googleBooksService = mock(GoogleBooksService.class);
    private final Consumer<List<Item>> consumer = mock(Consumer.class);
    private final TaskExecutor taskExecutorMock = mock(TaskExecutor.class);
    private final ItemService instance = new ItemService();

    public ItemServiceTest() {
        instance.itunesService = itunesService;
        instance.googleBooksService = googleBooksService;
        instance.itunesTaskExecutor = taskExecutorMock;
        instance.googleBooksTaskExecutor = taskExecutorMock;
    }

    @Test
    public void testSearch() {
        when(itunesService.search(eq(SEARCH_PHRASE)))
                .thenReturn(ITUNES_ITEMS);
        when(googleBooksService.search(eq(SEARCH_PHRASE)))
                .thenReturn(GOOGLEBOOKS_ITEMS);

        instance.search(SEARCH_PHRASE, consumer);
        
        ArgumentCaptor<Runnable> taskCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(taskExecutorMock, times(2)).execute(taskCaptor.capture());
        taskCaptor.getAllValues().forEach(runnable -> runnable.run());

        ArgumentCaptor<List<Item>> argumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(consumer).accept(argumentCaptor.capture());
        List<Item> expectedItems = SORTED_ITEMS;
        assertEquals(expectedItems, argumentCaptor.getValue());
    }

}
