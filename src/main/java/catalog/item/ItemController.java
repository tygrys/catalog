package catalog.item;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("/item")
public class ItemController {
    
    @Autowired
    ItemService itemService;
    @Value("${catalog.response.timeout}")
    Long responseTimeout;
    
    @GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public DeferredResult<List<Item>> search(@RequestParam String phrase) {
        DeferredResult<List<Item>> deferredResult = new DeferredResult<>(responseTimeout);
        
        Runnable consumTrigger = itemService.search(phrase, (items) -> {deferredResult.setResult(items);});
        deferredResult.onTimeout(() -> {consumTrigger.run();});
        
        return deferredResult;
    }

}
