package catalog.item;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

@Service
public class ItemService {

    @Autowired
    ItunesService itunesService;
    @Autowired
    GoogleBooksService googleBooksService;
    @Autowired
    TaskExecutor itunesTaskExecutor;
    @Autowired
    TaskExecutor googleBooksTaskExecutor;

    public Runnable search(String phrase, Consumer<List<Item>> consumer) {
        ItemAggregator aggregator = new ItemAggregator(consumer);
        itunesTaskExecutor.execute(() -> {
            if (!aggregator.areConsumed()) {
                try {
                    aggregator.aggregateItunesItems(itunesService.search(phrase));
                } catch (Exception exception) {
                    aggregator.aggregateItunesItems(Collections.emptyList());
                }
            }
        });
        googleBooksTaskExecutor.execute(() -> {
            if (!aggregator.areConsumed()) {
                try {
                    aggregator.aggregateGoogleBooksItems(googleBooksService.search(phrase));
                } catch (Exception exception) {
                    aggregator.aggregateGoogleBooksItems(Collections.emptyList());
                }
            }
        });
        
        return (() -> {aggregator.forceConsume();});
    }

}
