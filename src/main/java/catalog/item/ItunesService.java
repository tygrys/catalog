package catalog.item;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class ItunesService {

    @Autowired
    RestTemplate restTemplate;
    @Value("${catalog.search.limit}")
    Integer searchLimit;

    public List<Item> search(String phrase) throws RestClientException {
        UriComponents uriBuilder = UriComponentsBuilder
                .fromUriString("https://itunes.apple.com")
                .path("search")
                .queryParam("term", phrase)
                .queryParam("media", "music")
                .queryParam("entity", "album")
                .queryParam("limit", searchLimit)
                .build();
        ResponseEntity<ItunesResponse> itunesResponse = restTemplate.getForEntity(uriBuilder.toUri(), ItunesResponse.class);
        ArrayList<Item> items = new ArrayList<>();
        if (itunesResponse.getBody() != null && itunesResponse.getBody().getResults() != null) {
            for (ItunesResult result : itunesResponse.getBody().getResults()) {
                items.add(Item.builder().title(result.collectionName).author(result.artistName).type(Item.Type.ALBUM).build());
            }
        }
        return items;
    }

}
