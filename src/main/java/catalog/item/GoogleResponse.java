package catalog.item;
      
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor           
public class GoogleResponse
{
    private Integer totalItems;
    private GoogleItem[] items;
}