package catalog.item;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class GoogleBooksService {

    @Autowired
    RestTemplate restTemplate;
    @Value("${catalog.search.limit}")
    Integer searchLimit;
    
    public List<Item> search(String phrase) throws RestClientException {
        UriComponents uriBuilder = UriComponentsBuilder
                .fromUriString("https://www.googleapis.com")
                .path("books/v1/volumes")
                .queryParam("q", phrase)
                .queryParam("maxResults", searchLimit)
                .build();

        ResponseEntity<GoogleResponse> googleResponse = restTemplate.getForEntity(uriBuilder.toUri(), GoogleResponse.class);

        ArrayList<Item> items = new ArrayList<>();
        if (googleResponse != null && googleResponse.getBody() != null && googleResponse.getBody().getItems() != null) {
            for (GoogleItem item : googleResponse.getBody().getItems()) {
                if (item.getVolumeInfo() != null) {
                    items.add(Item.builder().title(item.getVolumeInfo().getTitle()).author(formatGoogleAuthors(item.getVolumeInfo().getAuthors())).type(Item.Type.BOOK).build());
                }
            }
        }
        return items;
    }

    private String formatGoogleAuthors(String[] authors) {
        if (authors == null) {
            return "";
        }
        
        StringBuilder formattedAuthors = new StringBuilder();
        for (int i = 0; i < authors.length; i++) {
            if (i > 0) {
                formattedAuthors.append(", ");
            }
            formattedAuthors.append(authors[i]);
        }
        return formattedAuthors.toString();
    }

}
