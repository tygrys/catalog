package catalog.item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ItemAggregator {

    private final Consumer<List<Item>> consumer;
    private List<Item> itunesItems;
    private List<Item> googleBooksItems;

    public synchronized void aggregateItunesItems(List<Item> items) {
        itunesItems = items;

        consumeIfAllItems(items);
    }

    public synchronized void aggregateGoogleBooksItems(List<Item> items) {
        googleBooksItems = items;

        consumeIfAllItems(items);
    }
    
    public synchronized void forceConsume() {
        if (itunesItems == null) {
            itunesItems = Collections.emptyList();
        }
        if (googleBooksItems == null) {
            googleBooksItems = Collections.emptyList();
        }
        consume();
    }
    
    public boolean areConsumed() {
        return itunesItems != null && googleBooksItems != null;
    }

    private void consumeIfAllItems(List<Item> items) {
        if (itunesItems != null && googleBooksItems != null) {
            consume();
        }
    }

    private void consume() {
        List<Item> aggregatedItems = new ArrayList<>(itunesItems);
        aggregatedItems.addAll(googleBooksItems);
        Collections.sort(aggregatedItems);
        consumer.accept(aggregatedItems);
    }

}
