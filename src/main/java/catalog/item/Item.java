package catalog.item;

import java.util.Comparator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Item implements Comparable<Item> {

    private String title;
    private String author;
    private Type type;

    @Override
    public int compareTo(Item otherItem) {
        return Comparator.comparing(Item::getTitle)
                .thenComparing(Item::getAuthor)
                .thenComparing(Item::getType).compare(this, otherItem);
    }

    public enum Type {
        BOOK,
        ALBUM
    }

}
