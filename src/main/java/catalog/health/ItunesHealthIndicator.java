package catalog.health;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ItunesHealthIndicator implements HealthIndicator {

    @AfterReturning("execution(* catalog.item.ItunesService.search(..))")
    public void afterReturningItunesSearch() throws Throwable {
        health = Health.up().build();
    }

    @AfterThrowing("execution(* catalog.item.ItunesService.search(..))")
    public void afterThrowingItunesSearch() throws Throwable {
        health = Health.down().build();
    }

    @Override
    public Health health() {
        return health;
    }

    private Health health = Health.up().build();

}
