package catalog.health;

import java.util.List;
import org.springframework.boot.actuate.health.OrderedHealthAggregator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

@Component
public class ApplicationHealthAggregator extends OrderedHealthAggregator {

    @Override
    protected Status aggregateStatus(List<Status> candidates) {
        return Status.UP;
    }

}
